
public class Ladung {
	private String bezeichnung;
	private int menge;

	public Ladung() {
		System.out.println("Standart");
	};

	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}

	
	/** 
	 * @return String
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}

	
	/** 
	 * @param bezeichnung
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	
	/** 
	 * @return int
	 */
	public int getMenge() {
		return menge;
	}

	
	/** 
	 * @param menge
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	};

	
	/** 
	 * @return String
	 */
	@Override
	public String toString() {
		return "[" + this.bezeichnung + " , " + this.menge + "]";
	};

}
