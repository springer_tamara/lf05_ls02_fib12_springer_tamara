import java.util.ArrayList;

public class Raumschiff {
	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
		System.out.println("Standart");
	};

	
	/** 
	 * @return ArrayList<Ladung>
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	
	/** 
	 * @param ladungsverzeichnis
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		super();
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = broadcastKommunikator;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.ladungsverzeichnis = ladungsverzeichnis;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.schiffsname = schiffsname;
		this.schildeInProzent = schildeInProzent;
	}

	String message;

	
	/** 
	 * @return String
	 */
	public String getSchiffsname() {
		return schiffsname;
	}

	
	/** 
	 * @param schiffsname
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	
	/** 
	 * @return int
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	
	/** 
	 * @param photonentorpedoAnzahl
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	
	/** 
	 * @return int
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	
	/** 
	 * @param energieversorgungInProzent
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	
	/** 
	 * @return int
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	
	/** 
	 * @param schildeInProzent
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	
	/** 
	 * @return int
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	
	/** 
	 * @param huelleInProzent
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	
	/** 
	 * @return int
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	
	/** 
	 * @param lebenserhaltungssystemeInProzent
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	
	/** 
	 * @return int
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	
	/** 
	 * @param androidenAnzahl
	 */
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	
	/** 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	
	/** 
	 * @param broadcastKommunikator
	 */
	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	
	/** 
	 * @param l1
	 */
	public void addLadung(Ladung l1) {
		this.ladungsverzeichnis.add(l1);
	}

	
	/** 
	 * @param r1
	 */
	public void photonentorpedoSchiessen(Raumschiff r1) {
		if (photonentorpedoAnzahl <= 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachrichtAnAlle("-=*Click*=-");
		} else {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r1);

		}
	}

	
	/** 
	 * @param r1
	 */
	public void phaserkanoneSchiessen(Raumschiff r1) {
		if (energieversorgungInProzent <= 50) {
			nachrichtAnAlle("-=*Click*=-");
		} else {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r1);

		}
	}

	
	/** 
	 * @param r1
	 */
	private void treffer(Raumschiff r1) {
		nachrichtAnAlle(schiffsname + " wurde getroffen!");
		schildeInProzent = schildeInProzent - 50;
		if (schildeInProzent <= 0) {
			huelleInProzent = huelleInProzent - 50;
			energieversorgungInProzent = energieversorgungInProzent - 50;
			if (huelleInProzent == 0) {
				lebenserhaltungssystemeInProzent = 0;
				nachrichtAnAlle("die Lebenserhaltungssysteme sind vernichtet worden");
			}

		} else {

		}
	}

	
	/** 
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}

	
	/** 
	 * @return ArrayList<String>
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;

	}

	
	/** 
	 * @return String
	 */
	@Override
	public String toString() {
		return "[" + this.schiffsname + " , " + this.photonentorpedoAnzahl + " , " + this.energieversorgungInProzent
				+ " , " + this.schildeInProzent + " , " + this.huelleInProzent + " , "
				+ this.lebenserhaltungssystemeInProzent + " , " + this.broadcastKommunikator + " , "
				+ this.ladungsverzeichnis + "]";
	}

}
