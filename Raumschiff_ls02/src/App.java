import java.util.ArrayList;

public class App {

        
        /** 
         * @param args
         */
        public static void main(String[] args) {
                Raumschiff vulkanier = new Raumschiff("NiVar", 0, 80, 80, 50, 100, 5, new ArrayList<String>(),
                                new ArrayList<Ladung>());
                Raumschiff klingonen = new Raumschiff("IKS", 1, 100, 100, 100, 100, 2, new ArrayList<String>(),
                                new ArrayList<Ladung>());
                Raumschiff romulaner = new Raumschiff("IRW", 2, 100, 100, 100, 100, 2, new ArrayList<String>(),
                                new ArrayList<Ladung>());
                Ladung l1 = new Ladung("Plasma Waffe", 50);
                Ladung l2 = new Ladung("Photonentorpedo", 3);
                Ladung l3 = new Ladung("Klingonen Schwert", 200);
                Ladung l4 = new Ladung("Ferengi", 200);
                Ladung l5 = new Ladung("Rote Materie", 2);
                Ladung l6 = new Ladung("Forschungsonde", 35);
                Ladung l7 = new Ladung("Borg-Schrott", 5);
                vulkanier.addLadung(l6);
                vulkanier.addLadung(l2);
                klingonen.addLadung(l3);
                klingonen.addLadung(l4);
                romulaner.addLadung(l5);
                romulaner.addLadung(l7);
                romulaner.addLadung(l1);
                System.out.println("Vulkanier: " + vulkanier);
                System.out.println("Klingonen: " + klingonen);
                System.out.println("Romulaner: " + romulaner);
                System.out.println("Alle Ladungen Vulkanier" + vulkanier.getLadungsverzeichnis());
                System.out.println("Alle Ladungen Klingonen" + klingonen.getLadungsverzeichnis());
                System.out.println("Alle Ladungen Romulaner " + romulaner.getLadungsverzeichnis());
                klingonen.photonentorpedoSchiessen(romulaner);
                romulaner.phaserkanoneSchiessen(klingonen);
                vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
                System.out.println("Klingonen: " + klingonen);
                System.out.println("Alle Ladungen Klingonen" + klingonen.getLadungsverzeichnis());
                klingonen.photonentorpedoSchiessen(romulaner);
                klingonen.photonentorpedoSchiessen(romulaner);
                System.out.println("Vulkanier: " + vulkanier);
                System.out.println("Klingonen: " + klingonen);
                System.out.println("Romulaner: " + romulaner);
                System.out.println("Alle Ladungen Vulkanier" + vulkanier.getLadungsverzeichnis());
                System.out.println("Alle Ladungen Klingonen" + klingonen.getLadungsverzeichnis());
                System.out.println("Alle Ladungen Romulaner " + romulaner.getLadungsverzeichnis());
                System.out.println(klingonen.eintraegeLogbuchZurueckgeben());
                System.out.println(vulkanier.eintraegeLogbuchZurueckgeben());
                System.out.println(romulaner.eintraegeLogbuchZurueckgeben());

        }

}