import java.util.Random;

public class Test {

    
    /** 
     * @param args
     */
    public static void main(String[] args) {

        System.out.println(myrandom(20, 30));

    }

    
    /** 
     * @param min
     * @param max
     * @return int
     */
    public static int myrandom(int min, int max) {
        Random rd = new Random();
        return min + rd.nextInt(max - min + 1);
    }
}